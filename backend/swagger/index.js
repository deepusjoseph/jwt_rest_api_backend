(function () {
    //top informatiom section 
    const info = require('./info.json')
    // const modules = require('./modules/index')
    //import project_industries
    const modules = require('./content.json')


    //import definitions
    //const definitions = require('./defintions/index')
    //footer section
    const footer = require('./footer.json')

    let swaggerPayload = {
        ...info,
        ...modules,
        // ...definitions
        ...footer
    }

    module.exports = swaggerPayload
})()