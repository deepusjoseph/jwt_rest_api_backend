const express = require('express');
const colors = require('colors');
const env = require('dotenv').config();
const connectDb = require('./config/db');
const { errorHandler } = require('./middleware/errorMiddleware')
const swaggerUi = require('swagger-ui-express')
const swaggerDocument= require('./swagger/index')

connectDb();
const app = express();

const port = process.env.PORT || 5000;

app.use(express.json());
app.use(express.urlencoded({ extended: false }))




app.use("/api/goals", require('./routes/goalRoutes'));
app.use("/api/users",require('./routes/userRoutes'))
app.use("/api/swagger-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use(errorHandler)



app.listen(port, () => console.log(`Server Started at ${port}`));