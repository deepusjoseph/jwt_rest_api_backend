const handleBars = require('express-async-handler');
const Goal = require('../models/goalModel');
const User = require('../models/userModel');
//@desc  get goals
//@route GET /api/goals/
//@access Private

const getGoals = handleBars(async (req, res) => {
    const goals = await Goal.find({ user: req.user.id })
    res.status(200).json(goals);
})
//@desc  set goals
//@route POST /api/goals/
//@access Private

const setGoals = handleBars(async (req, res) => {

    if (!req.body.text) {
        res.status(400);
        throw new Error('Please add a text field')
    }
    const goal = await Goal.create({
        text: req.body.text,
        user: req.user.id

    })
    res.status(200).json(goal)
})

//@desc  Update goals
//@route PUT /api/goals/:id
//@access Private

const updateGoals = handleBars(async (req, res) => {

    const goal = await Goal.findById(req.params.id)

    if (!goal) {
        res.status(400);
        throw new Error('Please provide id')
    }
    const user = await User.findById(req.user._id);

    //Check if the use exist
    if (!user) {
        res.status(400);
        throw new Error('User does not exist');
    }
    //Make sure the user update his own goal
    if (goal.user.toString() != user._id.toString()) {

        res.status(400);
        throw new Error('User not authorized');


    }

    const updatedGoal = await Goal.findByIdAndUpdate(req.params.id, req.body, {
        new: true
    })
    res.status(200).json(updatedGoal)
})

//@desc  Delete goals
//@route Delete /api/goals/:id
//@access Private

const deleteGoals = handleBars(async (req, res) => {
    const goal = await Goal.findById(req.params.id)
    if (!goal) {
        res.status(400);
        throw new Error('Please provide id')
    }
    const user = await User.findById(req.user._id);
    //Check if the use exist
    if (!user) {
        res.status(400);
        throw new Error('User does not exist');
    }
    //Make sure the user update his own goal
    if (goal.user.toString() != user._id.toString()) {
        res.status(400);
        throw new Error('User not authorized');


    }
    await goal.remove()
    res.status(200).json({ message: `Delete Goals ${req.params.id}` })
})

module.exports = {
    getGoals,
    setGoals,
    updateGoals,
    deleteGoals
}