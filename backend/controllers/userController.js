const handleBars = require('express-async-handler');
const User = require('../models/userModel');
const jwt = require('jsonwebtoken');
const bcrypt=require('bcryptjs');
//@desc  Register new user
//@route POST /api/users/
//@access Public

const registerUser =handleBars(async (req,res)=>{
    const {name,email,password}=req.body;

    if(!email || !name || !password ){
        res.status(400);
        throw new Error('Please add all fields')

    }
    //check if user already exist
    const userexists=await User.findOne({email});
    if(userexists){
        res.status(400);
        throw new Error('User Already exists')

    }
    //Bcrypting password 

    const saltPassword=await bcrypt.genSalt(10);
    const hashedPassword=await bcrypt.hash(password,saltPassword);

    //Create User
    const user = await User.create({
        name:name,
        email:email,
        password:hashedPassword
    })

    if(user){
        res.status(201).json({
            _id:user.id,
            name:user.name,
            email:user.email,
            token:generateJwt(user._id)
        })
    }else{
        res.status(400);
        throw new Error('Invalid user data')
    }
})

//@desc  Authenticate a user
//@route POST /api/users/
//@access Public

const loginUser = handleBars(async (req,res)=>{
    const {email,password}=req.body;
    //check for that email
    const user = await User.findOne({email});
    if(user && (await bcrypt.compare(password,user.password))){
        res.json({
            _id:user.id,
            name:user.name,
            email:user.email,
            token:generateJwt(user.id)
        });
    }else{
        res.status(400);
        throw new Error("Invalid credentials")

    }
    res.json({message:'Login User'})
})

//@desc  Get User Data
//@route GET /api/users/me
//@access Public

const getMe = handleBars(async (req,res)=>{
    const data = await User.find()
    res.json({data:data})
})

//Get JWT token
const generateJwt = (id)=>{
    return jwt.sign({id},process.env.JWT_SECRET,{
        expiresIn:'30d'
    })
}

module.exports={
    registerUser,
    loginUser,
    getMe
}