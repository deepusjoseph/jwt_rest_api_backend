const handleBars = require('express-async-handler');
const jwt = require('jsonwebtoken');
const User = require('../models/userModel');

const protect = handleBars(async (req,res,next)=>{
   
    let token;
    if(req.headers.authorization && req.headers.authorization.startsWith('Bearer') ){
        try{
            //GET token from headers
            token = req.headers.authorization.split(" ")[1];

            //Verify Token
            const decoded= jwt.decode(token,process.env.JWT_SECRET)

            //Get user from the token
            req.user=await User.findById(decoded.id).select('-password')

            next()

        }catch(error){
            res.status(401);
            throw new Error("User Not Authorized")

        }
    }
    if(!token){
        res.status(401)
        throw new Error("User Not Authorized")
    }

});

module.exports={protect};